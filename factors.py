#factors, multiples, lcm, gcf, prime factorization
import numpy as np
import random as rand

def factors (num):
    facs = []
    for i in range (num):
        if num % (i + 1) == 0:
            facs.append (i + 1)
    print (facs)

def multiples (n1, limit = 10): #i: 1 int, o: 1 int
	mults = []
	for i in range (1, limit):
		mults.append (n1 * i)
	return mults

def lcm (n1, n2, limit = 10): #i: 2 int, o: 1 int
  if n1 == n2:
    return n1
  mults = np.empty (shape = (0,2))
  for i in range (limit):#testing if lcm is between 1 and limit (ex: 10
    if n1 * (i + 1) in mults:
      return n1 * (i + 1)
    elif n2 * (i + 1) in mults:
      return n2 * (i + 1)
    mults = np.append (mults, [[n1 * (i + 1), n2 * (i + 1)]], axis = 0)

def gcf (n1, n2): #i: 2 int, o: 1 int
	#making factor list for both numbers
	n1facs = [i+1 for i in range (n1) if n1 % (i+1) == 0]
	n2facs = [i+1 for i in range (n2) if n2 % (i+1) == 0]
	print (n1facs)
	print (n2facs)
	for i in range ( min ( len (n1facs), len (n2facs) ) ):
		cntr = min ( len (n1facs), len (n2facs) ) - i
		if n1facs [cntr - 1] in n2facs:
			return n1facs [cntr-1]

def prime_factorization (n1): # 0 < n1 <= 100
	prime_list = [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97]
	not_p = n1
	prime_factors = []
	while not_p != 1: #?
		for each_prime in prime_list:
			if not_p % each_prime == 0:
				prime_factors.append (each_prime)
				not_p /= each_prime
	return prime_factors