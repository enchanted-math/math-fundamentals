import random as rand
import numpy as np

#from factors.txt
def lcm (n1, n2, limit = 10): #i: 2 int, o: 1 int
  if n1 == n2:
    return n1
  mults = np.empty (shape = (0,2))
  for i in range (limit):#testing if lcm is between 1 and limit (ex: 10
    if n1 * (i + 1) in mults:
      return n1 * (i + 1)
    elif n2 * (i + 1) in mults:
      return n2 * (i + 1)
    mults = np.append (mults, [[n1 * (i + 1), n2 * (i + 1)]], axis = 0)
#new code vvv

class Fraction:
	def __init__ (self, num, den):
		self.num = int (num)
		self.den = int (den)
		self.exp = "%s / %s" % (self.num, self.den)
    
	def simp_frac (f): #i: 1 Fraction obj, o: 1 Fraction obj
		for i in range ( min (f.num, f.den) ):
			x = min (f.num, f.den) - i
			if f.num % x == 0 and f.den % x == 0:
				return Fraction (f.num / x, f.den / x)

	def add_frac (f1, f2): #i: 2 Fraction obj, o: 1 Fraction obj
		sf1 = Fraction.simp_frac (f1)
		sf2 = Fraction.simp_frac (f2)
		den = lcm (sf1.den, sf2.den)
		sf1num = sf1.num * (den / sf1.den)
		sf2num = sf2.num * (den / sf2.den)
		num = sf1num + sf2num
		return Fraction (num, den)
	
	def sub_frac (f1, f2): #i: 2 Fraction obj, o: 1 Fraction obj
		sf1 = Fraction.simp_frac (f1)
		sf2 = Fraction.simp_frac (f2)
		den = lcm (sf1.den, sf2.den)
		sf1num = sf1.num * (den / sf1.den)
		sf2num = sf2.num * (den / sf2.den)
		num = sf1num - sf2num
		return Fraction (num, den)

	def mult_frac (f1, f2): #i: 2 Fraction obj, o: 1 Fraction obj
		num = f1.num * f2.num
		den = f1.den * f2.den
		return Fraction (num, den)

	def div_frac (f1, f2): #i: 2 Fraction obj, o: 1 Fraction obj
		return Fraction (f1.num * f2.den, f1.den * f2.num)

list1 = []

def dec_drill (decplace = 2):
	dec = round ( rand.random () , decplace )
	ans = Fraction.simp_frac ( Fraction ( dec * (10 ** decplace) , 100 ) )
	guess = input ( str (dec) + " " )
	print (ans.exp)
	list1.append (ans.den)
