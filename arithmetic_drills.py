#Arithmetic
import random as rand

def op_drill (minval, maxval, decplace = 0, terms_amount = 2):
    op = rand.choice (['+','-','*','/','^'])
    terms = []
    for i in range (terms_amount):
        terms.append (rand.randint (minval, maxval))
    if decplace != 0:
        for i in range (len (terms)):
            terms[i] += round (rand.random (), decplace)
    question = ""
    for i in range (len (terms)):
        if i == len (terms) - 1: #last term
            question += str (terms[i]) + " = "
        else:
            question += str (terms[i]) + " %s " % (op)
    ans = terms[0]
    if op == '+':
        for i in range (1, len (terms)):
            ans += terms [i]
    elif op == '-':
        for i in range (1, len (terms)):
            ans -= terms [i]
    elif op == '*':
        for i in range (1, len (terms)):
            ans *= terms [i]
    elif op == '/':
        try:
            for i in range (1, len (terms)):
                ans /= terms [i]
        except ZeroDivisionError:
            return
    else: # ^
        for i in range (1, len (terms)):
            ans = ans ** terms [i]
    guess = input(question)
    print (ans)

def perf_exp ():
    exp = rand.randint (2,3)
    if exp == 2:#practice perfect squares
        base = rand.randint (1,20)
    else: #practice perfect cubes
        base = rand.randint (1,10)
    guess = input ("{base} ^ {exp} = ".format (base = base, exp = exp))
    print (base ** exp)

def one_over_n ():
    n = rand.randint (1,10)
    guess = input ("1 / {n} = ".format (n = n))
    print (round (1 / n, 3))

def primes ():
    prime_list = [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97]
    true_num = rand.randint (0,100)
    if true_num in prime_list:
        ans = "p"
    else: #composite number
        ans = "c"
    guess = input ("{tn}? ".format (tn = true_num))
    print (ans)

def divisibility_rules (): #true 20-25% of the time
    sor = rand.choice ([2,3,4,5,6,9,10])
    dend = rand.randint (1,999)
    if dend % sor == 0:
        is_div = True
    else:
        is_div = False
    x = input ("%s / %s" % (dend, sor))
    print (is_div)