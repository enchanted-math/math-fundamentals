#unit conversion
#to practice often: length, temps
#less often: volume, mass
import random as rand

def length (): #km <-> mi, m <-> ft, cm <-> in
    way = rand.choice (['km2mi','mi2km','m2ft','ft2m','cm2in','in2cm'])
    if way == 'km2mi': #divide the length value by 1.609
        first_unit = rand.randint (1,100) #kilometers
        second_unit = round ( first_unit / 1.6 , 2 )
        guess = input ("%s kilometers to how many miles? " % (first_unit))
        print (second_unit)

    elif way == 'mi2km': #multiply the length value by 1.609
        first_unit = rand.randint (1,100) #miles
        second_unit = round ( first_unit * 1.6 , 2 )
        guess = input ("%s miles to how many kilometers? " % (first_unit))
        print (second_unit)

    elif way == 'm2ft': #multiply the length value by 3.281
        first_unit = rand.randint (1,100) #meters
        second_unit = round ( first_unit * 3.3 , 2 )
        guess = input ("%s meters to how many feet? " % (first_unit))
        print (second_unit)

    elif way == 'ft2m': #divide the length value by 3.281
        first_unit = rand.randint (1,100) #feet
        second_unit = round ( first_unit * 3.3 , 2 )
        guess = input ("%s feet to how many meters? " % (first_unit))
        print (second_unit)

    elif way == 'cm2in': #divide the length value by 2.54
        first_unit = rand.randint (1,100) #centimeters
        second_unit = round ( first_unit / 2.54 , 2 )
        guess = input ("%s centimeters to how many inches? " % (first_unit))
        print (second_unit)

    else: #in2cm, multiply the length value by 2.54
        first_unit = rand.randint (1,100) #inches
        second_unit = round ( first_unit * 2.54 , 2 )
        guess = input ("%s inches to how many centimeters? " % (first_unit))
        print (second_unit)

def area (): #sq km <-> sq mi, sq m <-> sq ft
    way = rand.choice (['sqkm2sqmi','sqmi2sqkm','sqm2sqft','sqft2sqm'])
    if way == 'sqkm2sqmi': #divide the area value by 2.59
        first_unit = rand.randint (1,100) #square kilometers
        second_unit = round ( first_unit / 2.6 , 2 )
        guess = input ("%s square kilometers to how many square miles? " % (first_unit))
        print (second_unit)

    elif way == 'smi2sqkm': #multiply the area value by 2.59
        first_unit = rand.randint (1,100) #square miles
        second_unit = round ( first_unit * 2.6 , 2 )
        guess = input ("%s square miles to how many square kilometers? " % (first_unit))
        print (second_unit)

    elif way == 'sqm2sqft': #multiply the area value by 10.764
        first_unit = rand.randint (1,100) #square meters
        second_unit = round ( first_unit * 10.8 , 2 )
        guess = input ("%s square meters to how many square feet? " % (first_unit))
        print (second_unit)

    else: #sqft2sqm, divide the area value by 10.764
        first_unit = rand.randint (1,100) #square feet
        second_unit = round ( first_unit / 10.8 , 2)
        guess = input ("%s square feet to how many square meters? " % (first_unit))
        print (second_unit)

def volume (): #liter <-> gallon
    way = rand.choice (['l2g','g2l'])
    if way == 'l2g': #divide the volume value by 3.785
        first_unit = rand.randint (1,100) #liters
        second_unit = round ( first_unit / 3.8 , 2 )
        guess = input ("%s liters to how many gallons? " % (first_unit))
        print (second_unit)

    else: #g2l, multiply the volume value by 3.785
        first_unit = rand.randint (1,100) #grams
        second_unit = round ( first_unit * 3.8 , 2 )
        guess = input ("%s gallons to how many liters? " % (first_unit))
        print (second_unit)

def mass (): #gram <-> pound
    way = rand.choice (['g2p','p2g'])
    if way == 'g2p': #divide the mass value by 453.592
        first_unit = rand.randint (1,100) #grams #set values
        second_unit = round ( first_unit / 453.6 , 2 )
        guess = input ("%s grams to how many pounds? " % (first_unit))
        print (second_unit)

    else: #p2g, multiply the mass value by 453.592
        first_unit = rand.randint (1,100) #pounds #set values
        second_unit = round ( first_unit * 453.6 , 2 )
        guess = input ("%s pounds to how many grams? " % (first_unit))
        print (second_unit)

def temperature ():
    way = rand.choice (['c2f','f2c'])
    if way == 'c2f':
        first_unit = rand.randint (1,100) #Celcius
        second_unit = round ( ( first_unit * (9/5) ) + 32 , 2 )
        guess = input ("%s degrees Celcius is how many degrees Fahrenheit? " % (first_unit))
        print (second_unit)

    else: #f2c
        first_unit = rand.randint (1,100) #Fahrenheit
        second_unit = round ( ( first_unit - 32 ) * (5/9) , 2 )
        guess = input ("%s degrees Fahrenheit is how many degrees Celcius? " % (first_unit))
        print (second_unit)

while True:
    fun_drills = rand.choice ([length,area,volume,mass,temperature])
    fun_drills ()