# % inc (tip), % dec (tax), commission

#calculate percentage increase or decrease from a starting value and a percentage
def change_per (start: 'float', per: 'float') -> 'float':
	return start + (start * per)

#calculate percentage inc/dec from a starting value and an end value
def change_2val (start: 'float', end: 'float') -> 'float':
	return ((end - start) / start) * 100

#calculate commission
def commission (start: 'float', per: 'float') -> 'float':
	return change_per (start, per) - start